<?php
function req($data) {
	$data = (is_array($data)) ? http_build_query($data) : $data; 

	$ch = curl_init();
	curl_setopt_array($ch, [
		CURLOPT_URL => 'https://saoneth.pl/phpcamp/api.php',
		CURLOPT_HTTPHEADER => [
			'User-Agent: Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36',
		],
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_POST => true,
		CURLOPT_POSTFIELDS => $data
	]);
	$content = curl_exec($ch);
	curl_close($ch);
	return $content;
}
//header('Content-type: text/plain');
$res = req([
	'action' => 'addProduct',
	'name' => 'frytki',
	'price' => 666
]);
$data = json_decode($res, true);
// Show as table
echo '<table>';
foreach($data as $key => $value)
	echo '<tr><th>'.$key.'</th><td>'.$value.'</td></tr>';
echo '</table>';

$res = req([
	'as' => 'xml',
	'action' => 'addProduct',
	'name' => 'frytki',
	'price' => 666
]);
$data = (Array) simplexml_load_string($res);
// Show as table
echo '<table>';
foreach($data as $key => $value)
	echo '<tr><th>'.$key.'</th><td>'.$value.'</td></tr>';
echo '</table>';