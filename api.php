<?php
header('Content-type: text/html; charset=utf-8');
// aBDDT6KEuJT4TxrP

try {
	$pdo = new PDO('mysql:unix_socket=/var/run/mysqld/mysqld.sock;dbname=phpcamp;encoding=utf8', 'phpcamp', 'aBDDT6KEuJT4TxrP');
	$result = [];
	switch($_REQUEST['action']) {
		// list all available actions 
		case '':
			$actions = ['checkProduct', 'addProduct', 'removeProduct'];
			$result['Message'] = 'Available actions: '.implode(', ', $actions);
			break;
		case 'checkProduct':
			// Check if id is set
			if(empty($_REQUEST['id']))
				throw new Exception('Missing parameter: id');
			
			// Check if id is number
			if(!ctype_digit($_REQUEST['id']))
				throw new Exception('Wrong data for parameter: id');
			$q = $pdo->prepare('SELECT `id`, `name`, `price` FROM `products` WHERE `id`=:id LIMIT 1');
			$q->bindValue(':id', $_REQUEST['id']);
			$q->execute();
			$result['product'] = $q->fetch(PDO::FETCH_ASSOC);
			break;
		case 'addProduct':
			// Check if name is set
			if(empty($_REQUEST['name']))
				throw new Exception('Missing parameter: name');
			
			// Check if price is set
			if(empty($_REQUEST['price']))
				throw new Exception('Missing parameter: price');

			// Check if price is vaild number
			if(!ctype_digit($_REQUEST['price']) && !preg_match('/^[0-9]+\.[0-9]{2}$/si', $_REQUEST['price']))
				throw new Exception('Wrong data for parameter: id');

			$q = $pdo->prepare('INSERT INTO `products` (`name`, `price`) VALUES (:name, :price)');
			$q->bindValue(':name', $_REQUEST['name']);
			$q->bindValue(':price', $_REQUEST['price']);
			$q->execute();
			$result['Message'] = 'New product id: '.$pdo->lastInsertId();
			$result['id'] = $pdo->lastInsertId();
			break;
		case 'removeProduct':
			// Check if id is set;
			if(empty($_REQUEST['id']))
				throw new Exception('Missing parameter: id');

			// Check if id is number
			if(!ctype_digit($_REQUEST['id']))
				throw new Exception('Wrong data for parameter: id');

			$q = $pdo->prepare('DELETE FROM `products` WHERE `id`=:id LIMIT 1');
			$q->bindValue(':id', $_REQUEST['id']);
			$q->execute();
			$result['Message'] = 'Affected rows: '.$pdo->rowCount();
			break;
		default:
			throw new Exception('Wrong action: '.$_REQUEST['action']);
	}
	$result['Error'] = 0;
} catch(Exception $e) {
	$result = [
		'Error' => 1,
		'Message' => $e->getMessage()
	];
}
function toxml($data, $root) {
	foreach($data as $k => $v) {
		if(is_array($v)) {
			toxml($v, $root->addChild($k));
		} else {
			$root->addChild($k, $v);
		}
	}
}
if(isset($_REQUEST['as']) && $_REQUEST['as'] == 'xml') {
	header('Content-type: application/xml');
	$xml = new SimpleXMLElement('<response/>');
	toxml($result, $xml);
	echo $xml->asXML();
} else {
	header('Content-type: application/json');
	echo json_encode($result);
}